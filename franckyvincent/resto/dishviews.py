# Create your views here.
from rest_framework import generics
from .models import PlatModel
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def create_dish(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        new_dish = PlatModel.objects.create(
            nom_plat=data['nom_plat'],
            description_plat=data['description_plat'],
            prix_plat=data['prix_plat']
        )
        return JsonResponse({'message': 'Plat créé avec succès'}, status=201)
    return JsonResponse({'error': 'Méthode non autorisée'}, status=405)

def get_all_dishes(request):
    dishes = PlatModel.objects.all()
    data = [{'nom_plat': dish.nom_plat, 'description_plat': dish.description_plat, 'prix_plat': dish.prix_plat} for dish in dishes]
    return JsonResponse(data, safe=False)

@csrf_exempt
def update_dish(request, dish_id):
    try:
        dish = PlatModel.objects.get(nom_plat=dish_id)
        if request.method == 'PUT':
            data = json.loads(request.body)
            dish.description_plat = data['description_plat']
            dish.prix_plat = data['prix_plat']
            dish.save()
            return JsonResponse({'message': 'Plat mis à jour avec succès'})
        return JsonResponse({'error': 'Méthode non autorisée'}, status=405)
    except PlatModel.DoesNotExist:
        return JsonResponse({'error': 'Plat non trouvé'}, status=404)
    
@csrf_exempt
def delete_dish(request, dish_id):
    try:
        dish = PlatModel.objects.get(nom_plat=dish_id)
        if request.method == 'DELETE':
            dish.delete()
            return JsonResponse({'message': 'Plat supprimé avec succès'})
        return JsonResponse({'error': 'Méthode non autorisée'}, status=405)
    except PlatModel.DoesNotExist:
        return JsonResponse({'error': 'Plat non trouvé'}, status=404)