# Create your views here.
from rest_framework import viewsets
import json
from .models import PlatModel, CommandeModel
from .serializers import PlatSerializer, CommandeSerializer, UserSerializer
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

class PlatListCreate(viewsets.ModelViewSet):
    queryset = PlatModel.objects.all()
    serializer_class = PlatSerializer


class CommandeListCreate(viewsets.ModelViewSet):
    queryset = CommandeModel.objects.all()
    serializer_class = CommandeSerializer
    permission_classes = [IsAuthenticated]

# class ClientListCreate(viewsets.ModelViewSet):
#     queryset = ClientModel.objects.all()
#     serializer_class = ClientSerializer
#     http_method_names = ['get','post']


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    http_method_names = ['get', 'post']


def login_view(request):
    if request.method == 'POST':
        try:
            # data = json.loads(request.body.decode('utf-8'))
            # username = data.get('username')
            # password = data.get('password')
            # Faites quelque chose avec le nom d'utilisateur et le mot de passe
            # Par exemple, vérifiez les informations de connexion
            #  et renvoyez une réponse JSON
            response_data = {'message': 'Login successful'}
            return JsonResponse(response_data)
        except json.JSONDecodeError:
            response_data = {'message': 'Invalid JSON data'}
            return JsonResponse(response_data, status=400)

    response_data = {'message': 'Invalid request method'}
    return JsonResponse(response_data, status=405)
