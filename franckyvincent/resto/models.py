from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class PlatModel(models.Model):
    nom_plat = models.CharField(max_length=50)
    description_plat = models.CharField(max_length=150)
    prix_plat = models.FloatField() 

# class ClientModel(models.Model):
#     mail_client = models.CharField(max_length=60, primary_key=True)
#     hashed_password = models.CharField(max_length=60)

class CommandeModel(models.Model):
    statut_commande = models.CharField(max_length=50)
    client_commande = models.ForeignKey(User, on_delete=models.CASCADE)
    liste_plat = models.ManyToManyField(PlatModel)