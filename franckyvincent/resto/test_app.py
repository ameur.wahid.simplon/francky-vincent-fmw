import json
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from django.contrib.auth.models import User
from .models import PlatModel, CommandeModel
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.authtoken.models import Token
import jwt
from rest_framework_simplejwt.tokens import RefreshToken


class DishTests(TestCase):
# POST /dishes : testez qu'il est possible de créer un plat et que le statut renvoyé est 201    
    def test_create_dish(self):
        data = {
            'nom_plat': 'Nouveau Plat',
            'description_plat': 'Description du plat',
            'prix_plat': 10.99,
        }
        response = self.client.post(reverse('create_dish'), json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    def setUp(self):
         self.client = APIClient()
         self.plat = PlatModel.objects.create(nom_plat='Plat 1', description_plat='Description du plat 1', prix_plat=9.99)

# GET /dishes/1 : testez qu'il est possible de récupérer un plat avec l'identifiant 1
    def test_get_dish(self):
        response = self.client.get(reverse('get_all_dishes'))  # Utilisez la vue appropriée pour récupérer tous les plats
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1) 
        

class AuthenticationTests(TestCase):

#POST /login : testez que la connexion d'un utilisateur, avec de mauvais identifiants, renvoie le statut 401
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword')

    def test_invalid_login(self):
        data = {
            'username': 'testuser',
            'password': 'wrongpassword',
        }
        response = self.client.post(reverse('login'), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

##PPOST /login : testez que la connexion d'un utilisateur, avec de bons identifiants, renvoie un JWT
    def test_valid_login(self):
        data = {
            'username': 'testuser',
            'password': 'testpassword',
        }
        response = self.client.post(reverse('login'), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)  # Expected status code is 200
        


class OrderTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword')

# POST /orders : testez que la route renvoie 403 si aucun JWT n'est fourni
    def test_create_order_no_jwt(self):
        response = self.client.post(reverse('command-list'), {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# POST /orders : testez que la route renvoie 403 si un JWT incorrect est fourni
    def test_create_order_invalid_jwt(self):
        invalid_jwt = 'invalid_token'
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + invalid_jwt)
        response = self.client.post(reverse('command-list'), {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    
# POST /orders : testez que la route renvoie 201 si un JWT correct est fourni

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        # self.jwt_token = jwt.encode({'username': self.user.username}, 'your_secret_key', algorithm='HS256')
        refresh = RefreshToken.for_user(self.user)
        self.access_token = str(refresh.access_token)

    def test_jwt_auth(self):
        # Utilisez le JWT pour accéder à un endpoint qui nécessite une authentification
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.access_token}')
        response = self.client.get(reverse('command-list'))

        # Assurez-vous que la requête avec le JWT est autorisée
        self.assertEqual(response.status_code, status.HTTP_200_OK)
