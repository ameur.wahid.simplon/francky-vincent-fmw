from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
import json

@csrf_exempt
def create_user_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']

        # Créer un nouvel utilisateur
        user = User.objects.create_user(username=username, password=password, email=email)

        # Authentifier et connecter l'utilisateur
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return JsonResponse({'no': 'Plat mis à jour avec succès'})  # Redirigez vers la page de votre choix après la connexion réussie

    return JsonResponse({'message': 'yes'})


@csrf_exempt
def login_view(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body.decode('utf-8'))
            username = data.get('username')
            password = data.get('password')
            
            # Vérification des informations de connexion
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)  
                
                response_data = {'message': 'Login successful'}
                return JsonResponse(response_data)
            else:
                response_data = {'message': 'Invalid username or password'}
                return JsonResponse(response_data, status=401)
            
        except json.JSONDecodeError:
            response_data = {'message': 'Invalid JSON data'}
            return JsonResponse(response_data, status=400)
    
    response_data = {'message': 'Invalid request method'}
    return JsonResponse(response_data, status=405)
