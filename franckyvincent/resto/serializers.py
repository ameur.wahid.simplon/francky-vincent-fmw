from rest_framework import serializers
from .models import * 
from django.contrib.auth.models import User

class PlatSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlatModel
        fields = '__all__'

# class ClientSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ClientModel
#         fields = '__all__'

class CommandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CommandeModel
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'password', 'username')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )
        return user