from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token
from .dishviews import create_dish, get_all_dishes
from django.contrib.auth import views as auth_views
from .userview import login_view
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

urlpatterns = [
    
]

from . import views

router = DefaultRouter()
router.register(r'plat', views.PlatListCreate)
router.register(r'user', views.UserViewSet)
router.register(r'command', views.CommandeListCreate, basename='command')


urlpatterns = [
    path('', include(router.urls)),
    # path('api/token/', obtain_auth_token,
    #      name='obtain-token'),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api-auth/', include('rest_framework.urls',
                              namespace='rest_framework')),
    path('openapi/', get_schema_view(
        title="Resto de francky",
        description="API",
        version="1.0.0",
    ),
        name='openapi-schema'),
    path('swagger-ui/', TemplateView.as_view(
        template_name='swagger_ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
    path('dishes/', create_dish, name='create_dish'),
    path('dishes/all', get_all_dishes, name='get_all_dishes'),
    path('login/', login_view, name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('password_change/', auth_views.PasswordChangeView.as_view(),
         name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(),
         name='password_change_done'),
    ]
